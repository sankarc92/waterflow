import { useState } from "react";
import "./App.css";

// const GridCreation = () => {
//   return (
//     <div className="app-container">
//       <h1>Waterflow simulator</h1>
//       <ReactSlider
//         className="horizontal-slider"
//         thumbClassName="example-thumb"
//         trackClassName="example-track"
//         renderThumb={(props, state) => <div {...props}>{state.valueNow}</div>}
//       />
//     </div>
//   );
// };

const ROWS = 5;
const COLUMNS = 5;
// const obstacles = 3;

const makeArr = (rows, columns) => {
  const arr = [];
  for (let i = 0; i < rows; i++) {
    arr[i] = [];
    for (let j = 0; j < columns; j++) {
      arr[i][j] = undefined;
      if (i === 0 && j === 0) {
        arr[i][j] = "obstacles";
      }
      if (i === 0 && j === 4) {
        arr[i][j] = "obstacles";
      }
      if (i === 2 && j === 1) {
        arr[i][j] = "obstacles";
      }
      if (i === 2 && j === 2) {
        arr[i][j] = "obstacles";
      }
      if (i === 2 && j === 4) {
        arr[i][j] = "obstacles";
      }
      if (i === 4 && j === 1) {
        arr[i][j] = "obstacles";
      }
      if (i === 4 && j === 3) {
        arr[i][j] = "obstacles";
      }
    }
  }
  console.log("new", arr);
  return arr;
};

const fillArr = (arr, row, startCol) => {
  if (arr[row][startCol] === "obstacles" || arr[row][startCol] === "water") {
    return;
  }
  let foundOb = false;
  for (let i = row; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
      if (j === startCol) {
        if (arr[i][j] === "obstacles") {
          fillArr(arr, i - 1, j - 1);
          fillArr(arr, i - 1, j + 1);
          foundOb = true;
          break;
        } else {
          if (!foundOb) arr[i][j] = "water";
        }
      }
    }
  }
  return arr;
};

// const ObstaclesView = () => {
//   return (
//     <div className="obstacles-conatiner">
//       <div></div>
//       <Draggable
//         handle=".handle"
//         defaultPosition={{ x: 0, y: 0 }}
//         position={null}
//         grid={[25, 25]}
//         scale={1}
//         onStart={() => {}}
//         onDrag={() => {
//           console.log("on drag");
//         }}
//         onStop={() => {
//           console.log("dragp stoperd");
//         }}
//       >
//         <div className="handle"></div>
//       </Draggable>
//       <div></div>
//     </div>
//   );
// };

const DisplayGrid = () => {
  const arr = makeArr(ROWS, COLUMNS);
  console.log("main arr", arr);

  const [mainBoard, setMainBoard] = useState(arr);

  const handleStart = (arr, row, col) => {
    if (row !== 0) {
      return;
    }

    let updatedBoard = fillArr(arr, row, col);
    setMainBoard([...updatedBoard]);
  };

  return (
    <div class="app-container">
      <div
        id="grid-container"
        style={{
          gridTemplateColumns: `repeat(${COLUMNS}, 1fr)`,
          gridTemplateRows: `repeat(${ROWS}, 1fr)`,
        }}
      >
        {mainBoard.map((row, rowIndex, arr) =>
          row.map((cols, colIndex) => (
            <div
              className={
                cols === "water" ? "blue" : cols === "obstacles" ? "black" : ""
              }
              onClick={() => handleStart(arr, rowIndex, colIndex)}
            ></div>
          ))
        )}
      </div>
      {/* <ObstaclesView /> */}
    </div>
  );
};

function App() {
  return <DisplayGrid />;
}

export default App;
